module ApplicationHelper
  
  # def nav_pages
  #   @nav_pages = Page.all
  # end
    
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "The GCD Gym"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

end

