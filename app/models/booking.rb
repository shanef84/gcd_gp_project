class Booking < ApplicationRecord
    
    belongs_to  :user, class_name: "User"
    belongs_to  :gym_classes, class_name: "GymClass"
    
end
