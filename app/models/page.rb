class Page < ApplicationRecord
  validates_uniqueness_of :permalink
  mount_uploader :image, ImageUploader
  
  has_one :gallery
  
  def to_param
    permalink
  end
end
