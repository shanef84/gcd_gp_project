class Gallery < ApplicationRecord
  validates_uniqueness_of :page_id
  belongs_to :page
  has_many :gallery_images, dependent: :destroy, foreign_key: :galleries_id
  has_many :images, :through => :gallery_images
end
