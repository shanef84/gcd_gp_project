class GymClass < ApplicationRecord
    

    has_many :bookings, :foreign_key => "class_no",
                        dependent:   :destroy
    
end
