class Image < ApplicationRecord
    
    mount_uploader :filename, ImageUploader
    
    has_many :gallery_images, dependent: :destroy, foreign_key: :images_id
    has_many :galleries, :through => :gallery_images
end
