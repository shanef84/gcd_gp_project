class Membership < ApplicationRecord
    
    has_many :users, :foreign_key => "member_type"
    
end
