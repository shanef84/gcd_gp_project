class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  
  before_filter :load_pages
  
  def index
    redirect_to '/pages/home'
  end
  
  def load_pages
    @pages = Page.all
    @memberships = Membership.all
    @gym_classes = GymClass.all
    @contact = Contact.new
  end
  
  def hello
    render html: "hello, world!"
  end
  
end
