class UsersController < ApplicationController
  
  before_action :logged_in_user,      only: [:index, :edit, :update]
  before_action :correct_user,        only: [:edit, :update]
  before_action :administrator_user,  only: :destroy
  
  def index
    @users = User.paginate(page: params[:page])
  end
  
  def show
    @user = User.find(params[:id])
    #debugger
  end
  
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      # Handle a successful save.
      log_in @user
      flash[:success] = "Welcome to the Griffth College Gym"
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    puts "params id = " + params[:id].to_s
    puts "member_type = " + params[:member_type].to_s
    
    if params[:member_type].present?
      puts "member_type is populated"
      @user = User.find(params[:id])
      @user.update_attribute(:member_type,params[:member_type])
      flash[:success] = "Membership registration succesful"
      redirect_to :back
    else
      @user = User.find(params[:id])
      if @user.update_attributes(user_params)
        # Handle a successful update.
        flash[:success] = "Profile updated"
        redirect_to @user
      else
        render 'edit'
      end
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end




  private

    def user_params
      params.require(:user).permit(:name, :email, :phone, :password, :password_confirmation, :member_type)
    end
    
    # Before filters
    
    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
    # Confirms an admin user.
    def administrator_user
      redirect_to(root_url) unless current_user.administrator?
    end
  
  
end
