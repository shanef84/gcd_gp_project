json.array!(@bookings) do |booking|
  json.extract! booking, :id, :name, :phone, :email, :membership_no, :class_no
  json.url booking_url(booking, format: :json)
end
