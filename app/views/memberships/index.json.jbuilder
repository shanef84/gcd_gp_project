json.array!(@memberships) do |membership|
  json.extract! membership, :id, :title, :description, :price
  json.url membership_url(membership, format: :json)
end
