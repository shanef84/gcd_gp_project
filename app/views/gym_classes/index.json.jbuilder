json.array!(@gym_classes) do |gym_class|
  json.extract! gym_class, :id, :name, :day, :time, :description, :instructor, :start_date
  json.url gym_class_url(gym_class, format: :json)
end
