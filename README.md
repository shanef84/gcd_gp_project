== README

##LIVE on HEROKU

https://whispering-river-87674.herokuapp.com/

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

## Information on the app

Shane Forbes
Gareth O'Connor
2017

Course:         BSCH Griffith College Dublin
Stage / Year:   4
Module:         Web Frameworks
Assignment No:   Group project

Requirement 
*   build a public website and 

*   website administration system for a fitness centre.

The website will have two parts:
*   The public website. It will have a minimum of four pages to include the following pages; home, membership, classes, contact us.
*   The second part will be used by the fitness centre owner to maintain the public webpages. It will be password protected.

1.1  *The home page will display (as will all other pages) a title, description and images

1.2  *The membership page will display (as will all other pages) a title, description and images. It will also display a list of membership options (for example Platinum, Gold, Off-Peak, Student/Over 60).
     *Each membership option will have a title, description and price

1.3  *The class timetable page will display a list of classes. Each class will have the following information: 
        Class name, the day of the week (eg Monday) along with the time, description, instructor name and class start date
     *From the public website, the user will be able to book a place on a class. Users will be able to book an individual class
     *When booking a class, the user must supply his/her 
        name, phone number, email address and MembershipNo (if the person is a member).
        
1.4  *The contact page will display a form through which the user will be able to send a message to the fitness centre owner. 
     *The user will be able to enter his/her name, phone number, email and a message.

2.1  Administration Page
    *   Every public page should be editable by the owner, i.e. he/she will be able to modify the title, text and images that will be displayed on each page.
        The fitness centre owner will be able to dynamically create and/or delete public webpages (but not: the home, membership, class timetable, and contact pages).
        
    *   The fitness centre owner will be able to create/read/update and delete membership options in the administration pages
    
    *   The administration pages will provide functionality to maintain the classes, (ie ability to Create, Read, Update and Delete a class).
        The administration pages should also be able to display for each class, details of all users who have booked that class. 
        The administration pages should also allow you to maintain the bookings ie Create, Read, Update and Delete a booking.
        
    *   The administration section will display all of the messages that have been submitted to the website.
    
    *   In the administration section the fitness centre owner will be able to create and modify an image gallery per public page. 
        For each image the following should be recorded: an id, filename, alternative text and caption. 
        There can be variable number of images per gallery. An image can be used in multiple pages.
        
    *   The administration section will be password protected with sessions and cookies used to restrictvaccess. 
        The fitness centre owner will be able to logon to this section from the public home page.
        All webpages can be administered from within this section

Shane Forbes
Gareth O'Connor
2017

<tt>rake doc:app</tt>.