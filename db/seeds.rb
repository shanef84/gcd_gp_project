# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if Page.count == 0
    Page.create!(name: "home", permalink: "home", content: "Editable home page content here", deletable: 'f')
    Page.create!(name: "memberships", permalink: "memberships", content: "Editable memberships page content here", deletable: 'f')
    Page.create!(name: "classes", permalink: "classes", content: "Editable classes page content here", deletable: 'f')
    Page.create!(name: "contact us", permalink: "contactus", content: "Editable contact us page content here", deletable: 'f')

end

if User.count == 0
    User.create!(name:  "Shane Forbes",
                 email: "shane.forbes@gcd.ie",
                 password:              "Thomas8287",
                 password_confirmation: "Thomas8287",
                 admin: 1,
                 administrator: true,
                 member_type: 2)

    20.times do |n|
      name  = Faker::Name.name
      email = "example-#{n+1}@gmail.ie"
      password = "password"
      User.create!(name:  name, email: email, password: password, password_confirmation: password, member_type: 1)
    end
end

if Membership.count == 0
     Membership.create!(title: "Non-Member", description: "Non-Member", price: 0)
    Membership.create!(title: "Platinum", description: "Unlimited classes included in price", price: 500)
    Membership.create!(title: "Gold", description: "Up to 10 classes included in price", price: 400)
    Membership.create!(title: "Off-Peak", description: "Book classes from Jan-Mar or June-Aug", price: 250)
    Membership.create!(title: "Student / Over 60's", description: "Unlimited classes included in price", price: 300)
   
end

if GymClass.count == 0
    GymClass.create!(name: "Yoga 6 classes", day: "Monday", time: "18:00", description: "From beginner to expert, one size fits all", instructor: "Anna-Marie Flemming", start_date: "2017-05-08")
    GymClass.create!(name: "Spin 6 classes", day: "Tuesday", time: "18:00", description: "From beginner to expert, one size fits all", instructor: "John Smith", start_date: "2017-05-09")
    GymClass.create!(name: "Tone 6 classes", day: "Wednesday", time: "18:00", description: "From beginner to expert, one size fits all", instructor: "Avril O'Brein", start_date: "2017-05-10")
    GymClass.create!(name: "Boxercise 6 classes", day: "Thursday", time: "18:00", description: "From beginner to expert, one size fits all", instructor: "Thomas Graham", start_date: "2017-05-11")
    GymClass.create!(name: "Yoga", day: "Monday", time: "18:00", description: "From beginner to expect, one size fits all", instructor: "Anna-Marie Flemming", start_date: "2017-05-08")
    GymClass.create!(name: "Spin", day: "Tuesday", time: "18:00", description: "Novice", instructor: "Thomas Graham", start_date: "2017-05-09")
    GymClass.create!(name: "Tone", day: "Wednesday", time: "18:00", description: "Novice", instructor: "Adam Cork", start_date: "2017-05-10")
    GymClass.create!(name: "Boxercise", day: "Thursday", time: "18:00", description: "Rough and Readt", instructor: "Adam Cork", start_date: "2017-05-11")
    GymClass.create!(name: "Dance", day: "Friday", time: "18:00", description: "fun", instructor: "John Smith", start_date: "2017-05-12")
    GymClass.create!(name: "Zumba", day: "Saturday", time: "18:00", description: "Fun", instructor: "John Smith", start_date: "2017-05-13")
    GymClass.create!(name: "Step Aerobics", day: "Monday", time: "20:00", description: "Easy", instructor: "Thomas Graham", start_date: "2017-05-08")
    GymClass.create!(name: "Intense", day: "Wednesday", time: "20:00", description: "Hard Core", instructor: "John Smith", start_date: "2017-05-10")
    GymClass.create!(name: "Starter", day: "Wednesday", time: "20:00", description: "Easy", instructor: "Avril O'Brein", start_date: "2017-05-10")
    GymClass.create!(name: "Weights", day: "Friday", time: "20:00", description: "Hard Core", instructor: "Avril O'Brein", start_date: "2017-05-12")
    GymClass.create!(name: "Cycling", day: "Friday", time: "20:00", description: "Hard Core", instructor: "Thomas Graham", start_date: "2017-05-12")
end

