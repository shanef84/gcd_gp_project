# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170505134157) do

  create_table "bookings", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.integer  "membership_no"
    t.integer  "class_no"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["class_no"], name: "index_bookings_on_class_no"
    t.index ["membership_no", "class_no"], name: "index_bookings_on_membership_no_and_class_no", unique: true
    t.index ["membership_no"], name: "index_bookings_on_membership_no"
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "galleries", force: :cascade do |t|
    t.string   "name"
    t.integer  "page_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["page_id"], name: "index_galleries_on_page_id"
  end

  create_table "gallery_images", force: :cascade do |t|
    t.integer  "galleries_id"
    t.integer  "images_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["galleries_id"], name: "index_gallery_images_on_galleries_id"
    t.index ["images_id"], name: "index_gallery_images_on_images_id"
  end

  create_table "gym_classes", force: :cascade do |t|
    t.string   "name"
    t.string   "day"
    t.string   "time"
    t.string   "description"
    t.string   "instructor"
    t.date     "start_date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "images", force: :cascade do |t|
    t.string   "filename"
    t.string   "alt"
    t.string   "caption"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memberships", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "price"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "pages", force: :cascade do |t|
    t.string   "name"
    t.string   "permalink"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "deletable"
    t.string   "image"
    t.index ["permalink"], name: "index_pages_on_permalink"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.integer  "admin"
    t.integer  "member_type"
    t.boolean  "administrator",   default: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
