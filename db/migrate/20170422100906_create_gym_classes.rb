class CreateGymClasses < ActiveRecord::Migration[5.0]
  def change
    create_table :gym_classes do |t|
      t.string :name
      t.string :day
      t.string :time
      t.string :description
      t.string :instructor
      t.date :start_date

      t.timestamps
    end
  end
end
