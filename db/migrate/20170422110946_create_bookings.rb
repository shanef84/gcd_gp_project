class CreateBookings < ActiveRecord::Migration[5.0]
  def change
    create_table :bookings do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.integer :membership_no
      t.integer :class_no

      t.timestamps
    end
    
    add_index :bookings, :membership_no
    add_index :bookings, :class_no
    add_index :bookings, [:membership_no, :class_no], unique: true
    
    
  end
end
