class CreateGalleryImages < ActiveRecord::Migration[5.0]
  def change
    create_table :gallery_images do |t|
      t.references :galleries, foreign_key: true
      t.references :images, foreign_key: true

      t.timestamps
    end
  end
end
