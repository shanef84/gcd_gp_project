class AddDeletableToPages < ActiveRecord::Migration[5.0]
  def change
    add_column :pages, :deletable, :boolean
  end
end
