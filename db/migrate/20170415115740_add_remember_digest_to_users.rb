class AddRememberDigestToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :remember_digest, :string
    add_column :users, :admin, :integer
    add_column :users, :member_type, :integer
  end
end
