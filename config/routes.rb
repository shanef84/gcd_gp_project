Rails.application.routes.draw do
 
  
  resources :gallery_images
  resources :images
  resources :galleries
  #get 'sessions/new'
  #get 'users/new'

  root 'application#index'
  get 'about'      => 'static_pages#about'
  get 'news'       => 'static_pages#news'
  
  get    'signup'  => 'users#new'
  post 'signup'    => 'users#create'
  
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  
  delete 'logout'  => 'sessions#destroy'

  resources :users
  resources :contacts
  resources :memberships
  resources :gym_classes
  resources :bookings
  resources :pages
  # You can have the root of your site routed with "root"
  # root 'application#hello'
  # end
  
end
